package com.hiro.gravitysensordemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import java.util.Locale;

/**
 * Main activity
 */
public class MainActivity extends AppCompatActivity
{
    DisplayView displayView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Lock screen
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        //Get display view
        displayView = findViewById(R.id.canvas);

        // Set up the sensors
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor gravitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        // Create a listener
        SensorEventListener gravitySensorListener = new SensorEventListener()
        {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent)
            {
                float x = sensorEvent.values[0];
                float y = sensorEvent.values[1];
                float z = sensorEvent.values[2];

                // Draw the arrow
                displayView.update(x, y);

                // Print the strengths
                TextView textViewX = findViewById(R.id.xValue);
                TextView textViewY = findViewById(R.id.yValue);
                TextView textViewZ = findViewById(R.id.zValue);

                String unit = "ms<sup>2</sup>";
                String xStr = "X: " + String.format(Locale.GERMANY, "%.2f", x) + unit;
                String yStr = "Y: " + String.format(Locale.GERMANY, "%.2f", y) + unit;
                String zStr = "Z: " + String.format(Locale.GERMANY, "%.2f", z) + unit;

                textViewX.setText(Html.fromHtml(xStr));
                textViewY.setText(Html.fromHtml(yStr));
                textViewZ.setText(Html.fromHtml(zStr));
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) { }
        };

        // Register the listener
        sensorManager.registerListener(
                gravitySensorListener,
                gravitySensor,
                SensorManager.SENSOR_DELAY_GAME);
    }
}
